var jwt = require('jsonwebtoken');

var all_tokens = {};
var adminSecret = "subhash.negi.909@gmail.com";

var generate_session = function(user_data,  req, res){
    var token = jwt.sign({data: user_data},
        adminSecret,
        { expiresIn: 60*60*3 }); // 3 hour
    all_tokens[token]=true;
    res.cookie('s_token', token);
    res.send({"STATUS": "SUCCESS"});
}

var signout = function(req, res){
    var token = getCookie(req.headers.cookie,'s_token')
    all_tokens[token] = false;
    res.cookie('s_token', "");
    res.send(JSON.stringify({success: true}));
};

var checktoken = function(req, res){
    var token = getCookie(req.headers.cookie,'s_token')
    jwt.verify(token,  adminSecret, function(err, decoded) {
        if(all_tokens[token] && decoded && decoded.data  && decoded.data.email){
            res.send({"STATUS": "SUCCESS", "data": decoded.data});
        }else{
            res.cookie('terc_token', "");
            res.send({"STATUS": "INVALID"});
        }
    });
};

function getCookie(cookie,cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

var export_obj = {};
export_obj.checktoken = checktoken;
export_obj.signout = signout;
export_obj.generate_session = generate_session;

module.exports = export_obj;