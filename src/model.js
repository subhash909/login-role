var db_connection = require('./DBConnection');
var auth_model = require('./Auth');

var login = function(req, res){
    var data = req.body || {};
    var email = data.email || "";
    var password = data.password || "";
    db_connection.serve_connectionPool.query('SELECT * FROM user_role WHERE email=? and password=?', [email, password], (err, result, fields)=>{
        if(err || result.length==0){
            console.log("No entry found");
            res.send({"STATUS": "ERROR", "MESSAGE": "NOT_EXIST"});
        }else{
            var t_u_data = result[0];
            var user_data = {email: t_u_data.email, user_name: t_u_data.username, phone_no: t_u_data.phone_no, role: t_u_data.role};
            auth_model.generate_session(user_data, req, res);
        }  
    });
}

var signup = function(req, res){
    var data = req.body || {};
    var email = data.email || "";
    var user_name = data.u_name || "";
    var phone_no = data.p_no || "";
    var password = "123456";
    db_connection.serve_connectionPool.query('SELECT * FROM user_role WHERE email IS NULL LIMIT 1', (err, result)=>{
        if(err || result.length==0){
            console("No entry found");
            res.send({"STATUS": "ERROR", "MESSAGE": "NOT_EXIST"});
        }else{
            var parms = [email, password, user_name, phone_no, result[0].id];
            db_connection.serve_connectionPool.query('UPDATE user_role SET email=?, password=?, username=?, phone_no=? WHERE id=?', parms, (err, update_res)=>{
                if(err && err.code=="ER_DUP_ENTRY"){
                    res.send({"STATUS": "ERROR", "MESSAGE": "DUPLICATE"});
                }else if(err){
                    res.send({"STATUS": "ERROR", "MESSAGE": "NEtWORK"});
                }else{
                    res.send({"STATUS": "SUCCESS", "data":{"pass": password, "email": email}});
                }
            });
        }
    });
}

var export_obj = {};
export_obj.login = login;
export_obj.signup = signup;
module.exports = export_obj;