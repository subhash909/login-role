const mysql = require('mysql');

var pool  = mysql.createPool({
    connectionLimit : 1,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : ''
});

module.exports.serve_connectionPool = pool;