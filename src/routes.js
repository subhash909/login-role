var model = require('./model');
var auth_model = require('./Auth');
module.exports = function(app){

    app.route('/login')
    .post(model.login);

    app.route('/signup')
    .post(model.signup);

    app.route('/session')
    .get(auth_model.checktoken);

    app.route('/signout')
    .get(auth_model.signout);

    app.route('/')
    .get((req, res)=>{
        res.sendFile('./../static/index.html');
    });
}