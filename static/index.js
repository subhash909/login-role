var HomePage = Backbone.View.extend({
	
	// bind events
	events: {
        'click #login_submit': 'login_submit_clicked',
        'click #sign_up_button': 'sign_up_button_clicked',
        'click #signout': 'signout_clicked'
	},

	// render the main template
	render: function(){
        $(this.el).html(this.login_template());
        this.check_session();
		return this;
    },

    signout_clicked: function(){
        $.ajax({
            'url': '/signout',
            'type': 'get',
            'success': function(){
                window.location.reload();
            },
            'error': function(){
                window.location.reload();
            }
        });
    },
    
    check_session: function(){
        var that = this;
        $.ajax({
            'url': '/session',
            'type': 'get',
            'success': function(data){
                if(data && data.STATUS && data.STATUS=="SUCCESS"){
                    $(that.el).find('#home_page').show();
                    var user_details = data.data;
                    $(that.el).find('#user_details').html(that.user_detail_template({u: user_details}));
                }else{
                    $(that.el).find('#login_signup_form').show();
                }
            },
            'error': function(err){
                $(that.el).find('#login_signup_form').show();
            }
        })
    },

    login_submit_clicked: function(event){
        var that = this;
        var email = $(that.el).find('#login_form').find("input[name='email']").val();
        var pass = $(that.el).find('#login_form').find("input[name='password']").val();
        var error_message = "";
        if(!email) error_message = "provide a valid email id";
        if(!pass) error_message = "provide a valid password";
        if(error_message){
            $(that.el).find('#login_error_message').text(error_message);
            $(that.el).find('#login_error_message').show();
        }else{
            $.ajax({
                'url': '/login',
                'type': 'POST',
                'data': {email: email, password: pass},
                'success': function(data){
                    if(data && data.STATUS && data.STATUS=="ERROR"){
                        var error_message = "Some network error occurred.";
                        if(data.MESSAGE=="NOT_EXIST"){
                            error_message = "Not Registered.";
                        }
                        $(that.el).find('#login_error_message').text(error_message);
                        $(that.el).find('#login_error_message').show();
                    }else if(data && data.STATUS && data.STATUS=="SUCCESS"){
                        window.location.reload();
                    }
                },
                'error': function(err){
                    $(that.el).find('#login_error_message').text("Some network error occurred.");
                    $(that.el).find('#login_error_message').show();
                }
            });
        }
    },

    sign_up_button_clicked: function(event){
        var that = this;
        var email = $(that.el).find('#signup_from').find("input[name='email']").val();
        var user_name = $(that.el).find('#signup_from').find("input[name='user_name']").val();
        var phone_no = $(that.el).find('#signup_from').find("input[name='phone_no']").val();
        var error_message = "";
        if(!email) error_message = "provide a valid email id.";
        if(!user_name) error_message = "provide a valid user name.";
        if(!phone_no) error_message = "provide a valid phone number.";
        if(error_message){
            $(that.el).find('#signup_error_message').text(error_message);
            $(that.el).find('#signup_error_message').show();
        }else{
            $.ajax({
                'url': '/signup',
                'type': 'POST',
                'data': {email: email, u_name: user_name, p_no: phone_no},
                'success': function(data){
                    if(data && data.STATUS && data.STATUS=="ERROR"){
                        var error_message = "Some network error occurred.";
                        if(data.MESSAGE=="NOT_EXIST"){
                            error_message = "No any role exist.";
                        }else if(data.MESSAGE=="DUPLICATE"){
                            error_message = "Email already registered.";
                        }
                        $(that.el).find('#signup_error_message').text(error_message);
                        $(that.el).find('#signup_error_message').show();
                    }else if(data && data.STATUS && data.STATUS=="SUCCESS"){
                        $(that.el).find('#login_signup_form').hide();
                        $(that.el).find('#message_page').show();
                        $(that.el).find('#message_page').html('<p>Login Email: '+data.data.email+' and password: '+data.data.pass+'</p><p><a href="/">login</a></p>');
                    }
                },
                'error': function(err){
                    $(that.el).find('#signup_error_message').text("Some network error occurred.");
                    $(that.el).find('#signup_error_message').show();
                }
            });
        }
    },

	// Main template
	login_template: _.template(''+
        '<div style="margin:20px;">'+
            '<div id="login_signup_form" style="display: none;">'+
                '<form class="form-inline" id="login_form" style="margin: 0 auto; width: 70%;">'+
                    '<label for="email" class="mr-sm-2">Email address:</label>'+
                    '<input type="email" class="form-control mb-2 mr-sm-2" name="email">'+
                    '<label for="pwd" class="mr-sm-2">Password:</label>'+
                    '<input type="password" class="form-control mb-2 mr-sm-2" name="password">'+
                    '<button type="button" class="btn btn-primary mb-2" id="login_submit">Log In</button>'+
                    '<p id="login_error_message" style="display:none; color: red;"></p>'+
                '</form>'+
                '<hr/>'+
                '<form id="signup_from" style="margin: 0 auto; width: 420px;">'+
                    '<div class="form-group">'+
                    '<label for="email">Email:</label>'+
                    '<input type="email" class="form-control" placeholder="Enter email" name="email">'+
                    '</div>'+
                    '<div class="form-group">'+
                    '<label for="user_name">User name:</label>'+
                    '<input type="text" class="form-control" placeholder="Enter User name" name="user_name">'+
                    '</div>'+
                    '<div class="form-group">'+
                    '<label for="phone_no">Contact No:</label>'+
                    '<input type="number" class="form-control" placeholder="Enter phone no" name="phone_no">'+
                    '</div>'+
                    '<p id="signup_error_message" style="display:none; color: red;"></p>'+
                    '<button type="button" class="btn btn-primary" id="sign_up_button">Sign Up</button>'+
                '</form>'+
            '</div>'+
            '<div id="message_page" style="display: none;"></div>'+
            '<div id="home_page" style="display: none; text-align: center;">'+
                '<h3>Welcome to home page.</h3>'+
                '<div id="user_details">'+
                '</div>'+
            '</div>'+
		'</div>'+
    ''),
    
    user_detail_template: _.template(''+
        '<p>Email: <%= u.email %></p>'+
        '<p>Username: <%= u.user_name %></p>'+
        '<p>Phone Number: <%= u.phone_no %></p>'+
        '<p>Role: <%= u.role %></p>'+
        '<div class="btn btn-primary" id="signout">Sign out</div>'+
    '')

});

// View rendering
new HomePage({el: $("#body")}).render();