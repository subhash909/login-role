var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var router = require('./src/routes');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('static'));
router(app);


http.createServer(app).listen(9555, ()=>{
    console.log("Server started sucessfuly on port 9555");
});